package arquitectonicos.dao.factories.implementations.postgresql;

import arquitectonicos.dao.models.Region;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;

import java.util.List;

/**
 * Created by luis on 23/03/18.
 */
public class RegionDaoTest {

    @Test
    public void readTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        List<Region> regiones = regionDao.read("WHERE region_id = 1");
        Assert.assertNotNull(regiones);
        for (Region region : regiones){
            System.out.println(region);
            Assert.assertNotNull(region);
        }
        //Assert.assertEquals(3, regiones.size());
    }

    @Test
    public void readByIdTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        Region region = regionDao.read(new Long("1"));
        Assert.assertNotNull(region);
        System.out.println(region);
    }

    @Test
    public void createTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        Region region = new Region(new Long("2"), "Mexico");
        regionDao.create(region);
        Region regionToTry = regionDao.read(new Long("2"));
        Assert.assertEquals(region, regionToTry);
    }

    @Test
    public void updateTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        Region region =  regionDao.read(new Long("4"));
        region.setName("Oceania");
        regionDao.update(region);
        Region regionToTry = regionDao.read(new Long("4"));
        Assert.assertEquals(region, regionToTry);

    }

    @Test
    public void deleteTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        regionDao.delete(new Long("2"));
    }
}
