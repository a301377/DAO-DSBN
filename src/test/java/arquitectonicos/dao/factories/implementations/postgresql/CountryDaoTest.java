package arquitectonicos.dao.factories.implementations.postgresql;

import arquitectonicos.dao.factories.interfaces.CountryDao;
import arquitectonicos.dao.models.Country;
import arquitectonicos.dao.models.Region;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CountryDaoTest {
    @Test
    public void readTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        CountryDao countryDao = new arquitectonicos.dao.factories.implementations.countryDao.CountryDao(regionDao);

        List<Country> countries = countryDao.read("WHERE country_id = 1");
        Assert.assertNotNull(countries);
        for (Country country : countries){
            System.out.println(country);
            Assert.assertNotNull(country);
        }

    }

    @Test
    public void readByIdTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        Region region = regionDao.read(new Long("1"));
        Assert.assertNotNull(region);
        System.out.println(region);
    }

    @Test
    public void createTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        CountryDao countryDao = new arquitectonicos.dao.factories.implementations.countryDao.CountryDao(regionDao);

        Country country = new Country(new Long("2"), "Mexico", );
        countryDao.create(country);
        Country countryToTry = countryDao.read(new Long("2"));
        Assert.assertEquals(country, countryToTry);
    }

    @Test
    public void updateTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        Region region =  regionDao.read(new Long("4"));
        region.setName("Oceania");
        regionDao.update(region);
        Region regionToTry = regionDao.read(new Long("4"));
        Assert.assertEquals(region, regionToTry);

    }

    @Test
    public void deleteTest(){
        arquitectonicos.dao.factories.interfaces.RegionDao regionDao = new RegionDao();
        regionDao.delete(new Long("2"));
    }
}
