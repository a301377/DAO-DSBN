package arquitectonicos.dao.factories.implementations.postgresql;

import arquitectonicos.dao.conecctions.Conexion;
import arquitectonicos.dao.models.Location;
import arquitectonicos.dao.models.Region;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LocationDAO implements arquitectonicos.dao.factories.interfaces.LocationDAO {
    @Override
    public void create(Location obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(Location.INSERT);
            Integer i = 1;
            ps.setLong(i++, obj.getId());
            ps.setString(i++, obj.getAddress());
            ps.setInt(i++, obj.getCodigoPostal());
            ps.setString(i++, obj.getCiudad());
            ps.setString(i++, obj.getProvincia());
            ps.setLong(i++, obj.getCountry().getId());
            ps.executeUpdate();
        }catch (ClassNotFoundException | SQLException ex){

        }
    }

    @Override
    public void update(Location obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(String.format("%s, %s", Location.UPDATE, obj.getId()));
            Integer i = 1;
            ps.setLong(i++, obj.getId());
            ps.setString(i++, obj.getAddress());
            ps.setInt(i++, obj.getCodigoPostal());
            ps.setString(i++, obj.getCiudad());
            ps.setString(i++, obj.getProvincia());
            ps.executeUpdate();
        }catch (ClassNotFoundException | SQLException ex){

        }
    }


    @Override
    public List<Location> read(String criteria) {
        List<Location> Locations = new ArrayList<>();
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConn().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s",Location.Q_ALL, criteria));
            while(rs.next()){
                Locations.add(makeLocation(rs));
            }
        }catch (ClassNotFoundException | SQLException ex){

        }
        return Locations;
    }

    @Override
    public Location read(Long id) {
        Location Location = null;
        try {
            Connection conexion = Conexion.getInstance().getConn();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s",Location.Q_BY_ID, id));
            if(rs.next()){
                Location = makeLocation(rs);
            }
        }catch (ClassNotFoundException | SQLException ex){

        }
        return Location;
    }


    @Override
    public void delete(Long id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(Location.DELETE);
            Integer i = 1;
            ps.setLong(i++, id);
            ps.executeUpdate();
        }catch (ClassNotFoundException | SQLException ex){

        }
    }

    private Location makeLocation(ResultSet rs) throws SQLException {
        Location Location = new Location();
        Integer i = 1;
        Location.setId(rs.getLong(i++));
        Location.setAddress(rs.getString(i++));
        Location.setCodigoPostal(rs.getInt(i++));
        Location.setCiudad(rs.getString(i++));
        Location.setProvincia(rs.getString(i++));
        return Location;
    }
}
