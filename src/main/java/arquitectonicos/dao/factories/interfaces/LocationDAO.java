package arquitectonicos.dao.factories.interfaces;

import arquitectonicos.dao.models.Country;
import arquitectonicos.dao.models.Location;

import java.util.List;

public interface LocationDAO {

    public void create(Location obj);

    public List<Location> read(String criteria);

    public Location read(Long id);

    public void update(Location obj);

    public void delete(Long id);
}
