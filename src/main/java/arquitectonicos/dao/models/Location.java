package arquitectonicos.dao.models;

public class Location extends Model {
    public static final String FIELDS = "location_id, street_address, postal_code, city, state_province, country_id";
    public static final String TABLE = "locations";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE location_id = ", Q_ALL);
    public  static final String INSERT = String.format("INSERT INTO %s (%s) VALUES%s", TABLE, FIELDS, fieldsToInsert(6));
    public static final String UPDATE = String.format("UPDATE %s SET location_id = ?, country_name = ?, location_id = ? WHERE region_id =", TABLE);
    public static final String DELETE = String.format("DELETE FROM %s WHERE location_id = ?", TABLE);

    private Long id;
    private String address;
    private Integer codigoPostal;
    private String ciudad;
    private String provincia;
    private Country country;

    public Location() {
    }

    public Location(Long id, String address, Integer codigoPostal, String ciudad, String provincia, Country country) {
        this.id = id;
        this.address = address;
        this.codigoPostal = codigoPostal;
        this.ciudad = ciudad;
        this.provincia = provincia;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
