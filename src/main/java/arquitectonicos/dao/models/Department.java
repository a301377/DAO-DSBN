package arquitectonicos.dao.models;

public class Department extends Model{

    public static final String FIELDS = "";
    public static final String TABLE = "departments";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE deparment_id = ", Q_ALL);
    public  static final String INSERT = String.format("INSERT INTO %s (%s) VALUES%s", TABLE, FIELDS, fieldsToInsert(6));
    public static final String UPDATE = String.format("UPDATE %s SET department_id = ? WHERE region_id = ", TABLE);
    public static final String DELETE = String.format("DELETE FROM %s WHERE department = ?", TABLE);

    private Long id;
    private String name;
    private Employee managerId;
    private Location location;



}
